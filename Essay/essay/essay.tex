% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{a4paper}
\usepackage{graphicx}
\usepackage{paralist}
\usepackage{courier}
\usepackage{hyperref}
\usepackage{color}
\usepackage{titling}
\usepackage{array}
\usepackage{listings}
\usepackage{amsmath}

\usepackage[style=numeric-comp]{biblatex}
\bibliography{essay}

\lstnewenvironment{code}[1][]%
  {\noindent\minipage{\linewidth}\medskip 
   \lstset{basicstyle=\ttfamily\footnotesize,frame=single,#1}}
  {\endminipage}

\hypersetup{pdfborder={0 0 0},colorlinks=false,linkcolor=blue,urlcolor=blue,citecolor=blue}


\usepackage{sectsty}
\allsectionsfont{\rmfamily\mdseries\upshape}

\setlength{\droptitle}{-10em}
\title{Master Essay \\ \large 3D Matching algorithms}
\author{Matias Snellingen (\emph{matiassn}) \\
\url{matiassn@ifi.uio.no}}

%code quote
%\begin{lstlisting}[basicstyle=\scriptsize]
%\end{lstlisting}
\begin{document}

\begin{titlepage}
\maketitle
\vfill
\centering
\includegraphics[width=10cm]{uio.png}
\vfill
\vfill
\end{titlepage}
\tableofcontents

\newpage

\setlength{\parindent}{4em}
\setlength{\parskip}{1em}
\renewcommand{\baselinestretch}{1.5}


\section{Summary}

\subsection{About the thesis}
DNV-GL is a certification body and classification society and performs technical inspections and validation under, for example, a vessel under construction. This project will focus on the development of a virtual reality application that will make the inspection process more efficient, as currently these kinds of inspections are done with 2D drawings.

\subsection{The problem to solve}
When inspecting a construction, the inspector needs to find the correct drawings to write down what must be corrected. This process is time consuming and requires a fair amount of preparation before the inspection can start. By using augmented reality and matching the construction to be inspected with a 3D representation and tracking, the inspector can move around freely in both the real and virtual representation of the construction. Here the inspector can annotate the corrections and improvements by simply pointing/gesture towards what he sees and add the note. This requires a good 3D matching algorithm, tracking and 3D reconstruction of the real environment.

\subsection{Goal}
To develop an application that can match a reconstructed 3D environment with a virtual 3D model for augmented reality. The thesis will focus on researching and creating an algorithm that can accomplish this task with high enough accuracy and not to be too computationally heavy so that it can be used on a mobile device in the future, but for this project it is enough that it is able to fit into a car.

\subsection{What it requires me to do}
The first step is to recognize the problem and understand what problems I might encounter researching this. First, I need to understand how I am going to use the matching algorithms, what kind of scenes, resolution, noise and surfaces it needs to be able to handle. I need to create a list of criteria that the final algorithm needs to be able to handle for the matching to work as intended.  I need to find a few different alternative algorithms so that I can evaluate which one is best for the job. This requires to look at several published articles on the subject of 3D matching that seem promising for this project. When I have a few algorithms that I can use, I need to get some sample data that I can work with and try to implement these algorithms and compare their effectiveness based on the sample data. If none of the algorithms satisfy the requirements or are to complicated to implement, I need to rethink my approach and maybe change the scope and evaluate if this project is achievable with the current goals. If an implementation satisfies the requirements, I will need to start thinking about how I am going to implement this solution to be as effective as possible for real usage. This includes to find out how I am going to reconstruct the environments, should I use Microsoft Kinect, laser scan or other means. It will also be interesting if I am able to scale it down to be able to run on an embedded device like the Microsoft Hololens. 

\section{Scope of the thesis}
This thesis will focus mostly on core algorithms needed for the 3D matching, so there will not be much focus on how to the user interaction should be and how to implement it on an augmented reality device.
\begin{itemize}
\item What I will focus on: The focus will mainly be to implement a set of candidate algorithms that might work and compare these to each other and find the ones that works best for the intended purpose. I will also need to evaluate if they will be precise and fast enough to actually be useful for our project. It is also possible to use a combination of the different algorithms to increase the performance if possible. 
\item What I can do if I get more time: If I have identified and implemented an algorithm that works well, I can start implementing it with an augmented reality device and create a working prototype. If there is still no algorithm that works good enough, I can identify its shortcomings and try to find other possible solutions to the problem for further development with this project. 
\end{itemize}
\section{Identify the requirements}
The main requirements I need to look into when searching for an appropriate algorithm can be separated into 6 categories: 
\begin{itemize}
\item Performance: the algorithm is intended to be able to run on a mobile device that has limited performance. To start with, I am going to accept that it will require more than that, but should have a goal that it is possible to optimize it further. I also have to take into consideration that the matching will not be done with simple models but rather an complex environment with potentially high data density.
\item Time: the algorithm must not require too much time to actually complete the task as this is going to be used in a real-time application. Because of that I do not have the luxury of waiting a day or even 10 minutes for the computation to complete. So it is important how the algorithm's time complexity scales with the model complexity. 
\item Space: Since I aim for this code to be able to run on a mobile device with limited resources, the code can not use too much space as this is also quite limited. If the algorithm is robust enough, the solution can also be to scale down the density of the model that is going to be compared.
\item Precision: The precision of the algorithm needs to be good enough that the user can actually make accurate comparisons between the virtual model and the real environment. It needs to be able to scale, translate and rotate the matched objects accurately both with large scales and small scales. 
\item Complexity: the algorithm must also not be too complex, as I have a limited time to implement the algorithm. If it too complex and difficult to implement, I will not have enough time to implement the algorithms to test them out. They can also not rely on a complicated setup or infrastructure for it to work as intended.
\item Robustness: Since I scan a real environment, the scanned model will have occlusion, anomalies and broken surfaces that the algorithm needs to be able to handle. This is important as there is no way for us to create a fully complete and error free reconstructed model of the environment to do matching against.
\end{itemize}
\subsection{Full matching or partial marching}
I need to take into consideration what the algorithm should do a matching on. One way is to match the scanned environment, which is a small segment of a larger structure, and do a matching against a model of the whole structure that contains this segment. This approach has the advantage of only needing one large model that can be used to match several environments. This approach will require less preparations and gives the user more flexibility with what he wants to do a matching on. The downside, however, is that this requires vastly more computational power and can introduce considerable more false positives with similar areas of the large model. The other approach is to only do the matching against a preselected section of the complete model. The partial matching approach is faster and less error prone as there is less data to do the matching against and less false matches. The downside is that you have to prepare the selected part you want to match before you can start. Based on the clear advantages the partial matching approach has, I find this to be the best way of doing the matching as the preparation part is not an issue for our use case.

\subsection{Performance}
Performance is important if this should run on an augmented reality device like the Microsoft Hololens as it is a 
mobile device with limited performance. However, as this is a master project with limited time and resources, I am first going to focus
on researching and implementing an algorithm that shows promise, without too much focus on whether it will be able to run fast enough on a mobile device, but rather that if it is possible to achieve in a real time. The algorithm either has to be fault tolerant enough to be able to handle scans or reconstructions with low enough fidelity or fast enough to be able to handle it with high enough fidelity as scans with high fidelity can be a lot of data to process. 

\subsection{Precision}
This is primary goal and the most important factor for the algorithms I research, as these algorithms needs to be precise enough to be useful. This is because the final application has the goal to be used for inspections of vessels under construction. The algorithm has to be able to scale and orient the matched model precisely enough so that it can be useful to compare measurements and details. The algorithm also needs to be able to tackle the matching of objects that have anomalies, occlusions, noise and other artifacts. The reason for the requirement of robustness is that the captured and reconstructed models it will work with will have these issues. 

An interesting thing is to look at is how these algorithms will handle the matching when most, if not all, out scenarios will require matching
to be done from the inside of a model. So these algorithm will need to be capable to do the matching inside out with convex models.
It is not common do matching this way, most matching is done with a object scanned from the outside, like a cup or a small object.
In our case, the scan and reconstruction of the environment will only have a perspective from the inside of area and an incomplete coverage of the whole environment. Normally 3D matching is done from the outside of the model and with a more complete view, it will be interesting to see how the algorithms handles doing matching from the inside of an convex model. 
 
Scaling related problems will also probably be an issue when the algorithms will be doing matching with a large environment with small scale precision. When doing scaling according to the small features in the scene, it will likely have problems and incompatibilities with the larger features. The same problem also occurs when doing the reverse, where we start with the large features. I will see how well these the different algorithms handle this after I get some implementations done.

\section{Researching algorithms}
A lot of work is going to be spent on reading and finding algorithms that can be used for this project. There are a lot of matching algorithms out there and it is a well researched topic, however I am not doing 3D matching in the traditional way where you simply try to identify an object like a can. I want to prepare the algorithm so that it can match and overlay a virtual model on a real environment with augmented reality in the future. This gives me some challenges when searching for papers and algorithms on the topic, as I am not interesting in simple object recognition. When looking for candidate algorithms that I can build upon, it is important that the algorithms are able to satisfy the criteria mentioned above. Even if I evaluate, read and select the algorithms and they seem promising, it is still only an indication, I have to implement and test the most promising ones before I can say if they really satisfy all the requirements. Therefore the main question I need to ask about the algorithms is if they are too complex, because of our limited time frame, the algorithm should not be too complicated to understand or implement. It is also important that the algorithm is compatible with the intended purpose and that the algorithm is complete enough to be implemented. I am also interesting in finding more than one algorithms to the problem so that I can do a comparison of their performance, therefore I am interested in algorithms that are distinct from each other. When I look for algorithms, it is good to keep an eye out for names of authours that you come over again and again. One of these names is Funkhouser, and I see that many algorithm derives from work he has been involved with.

\section{Candidate algorithms}
I have selected 3 main algorithms that I want to implement (4 if both Correspondence clustering algorithms are counted). These have been selected by reading papers on 3D matching, seen what other projects that implement 3D matching use and other projects that have been similar to the one I have. The algorithms are distinct enough from each other so that comparing them becomes meaningful. They all are supposed to be able to handle both scanned point cloud data from the environment and the 3D virtual model of the environment so that it is possible to compare them.

\subsection{Shape distribution}
The shape distribution algorithm \cite{shapedist} is based on creating and shape distribution signature of the model that you can use for the comparison.
From the article about shape distribution, I see that this algorithm satisfy the requirements. Creating the shape signatures is $S\log{N}$ where $S$ are samples for a shape function that measure the distance between two random points on the surface, and $N$ is the amount of triangles. When you have the shape distribution, you can use functions that have a constant complexity and comparison cost. The algorithm claim that it is robust because of its random sampling and will cause this algorithm to be insensitive to small perturbations. The algorithm is also general enough that it should work for our project. 

\subsection{Hausdorff Distance}
This is an algorithm that I will use as an benchmark to measure the performance of the other algorithms. Running this algorithm on points or segments should have a time complexity of $\mathcal{O}(n^6\log{}n)$ with an exact algorithm. This is not really promising when I will probably do a comparison with high fidelity scans that contains a lot of data. There are approximate algorithms that show a more promising time complexity around $\mathcal{O}(n\log{}n)$ \cite{haus}. It not certain if these approximation algorithms are adequate for the requirements of this project.

\subsection{Correspondence clustering}
Correspondence clustering is what Point Cloud Library (PCL) \cite{pcl} uses for their object recognition. There are two different approaches that I will test, 3D Hough voting scheme, based on F. Tombari and L. Di Stefano: “Object recognition in 3D scenes with occlusions and clutter by Hough voting” \cite{hough}. The other approach is named geometric consistency and is based on H. Chen and B. Bhanu: “3D free-form object recognition in range images using local surface patches” \cite{gc}. Both algorithms seem to be tolerant to occlusion and cluttering and as the code is open source, it should not be a big deal to implement these algorithms to test. This is probably the first algorithm to test out because of its accessibility. 

\section{Potential use of 2D matching}
There is also the potential to look into if this problem can be reduced to a 2D matching problem. If I can do this in 2D, it greatly lowers the
complexity of the problem, however it is uncertain if I will be able to acquire the required accuracy and fault tolerance necessary. The focus is mainly going to revolve around the 3D problem and not the 2D, but it is still interesting to look into the possibility. One of the alternatives that I have considered is to generate images from the virtual model with accurate texture and lighting, then take a picture from the real environment and then run a matching algorithm on the two images instead of the two models. If it is not feasible to use 2D matching for a full match, it can be used for an approximation and then do the fine-tuning with one of the other algorithms. 

\section{Challenges}

\subsection{Complexity}
The algorithms are complex a computationally heavy, this requires to have a deep understanding of the algorithms and how to handle 3D graphical data efficiently and correctly. Since these algorithms also can be hard to understand completely, it can be difficult to see if they actually are able to achieve what you want them to do before you have a working implementation to test with. The implemented algorithms also need to be able to do several things, first of all the algorithms must to be able to identity and recognize the environment characteristics, then the algorithms need to be able to transform, scale and rotate the virtual model to match the environment. 

\subsection{Reconstruction}
The scanning and reconstruction of the environment should be of a good enough quality so that the algorithms are able to do accurate matching with the data. This might give us some challenges with areas that have a lot of occlusion, errors and environment with too few distinct details to separate it from others. Depending on how the environment is structured, you can end up with large occlusions that might make it difficult to do a correct matching. This can be solved by having multiple points where the user scans the environment, however this will drastically increase the data size that the algorithm needs to handle, it also makes the set-up procedure more complicated. It will also be interesting to see how well the depth image from an Microsoft Hololens or Kinect work for these algorithms as they have a lower resolution than a laser scan, but it is also much faster and efficient to use.

\subsection{Data size}
Having accurate scans is not necessarily a good thing as the density of the point cloud created from a laser scan can be too large. 3D point cloud scans from lasers contain an incredible density that requires a lot of processing to handle. If they are too large one might need to downscale it, but by doing so you also lose details that may be essential to get an accurate match. It will be important to find a middle ground and find a suitable way to create the ideal size and density for the scans. To improve the accuracy, the algorithm might need scans from different angels, and this gives us multiple scans and if they are too large it will become a problem. 

\subsection{Precision}
 Doing translation, rotation and scaling precisely based on a matched 3D models requires the algorithms to be very precise. The scaling might also be a challenging to do on small details and keeping this correct with larger parts of the scene as well and vice versa. A scenario like this can be to align the scaling of a whole room with both small objects and the room itself and at the same time and still keep it accurate for both. There is also the possibility to look into doing this scaling more dynamically by making it context dependent, but this is for later in the development when we have a valid match we can work with.

\subsection{Time}
Since this is a project that has a limited time frame and with such a complex project, some things might need to change to make the focus be more narrow on a more specific requirement. This can be to simply implement only the matching algorithm part of the project and leave the implementation for further development.

\section{Other remarks}
There are tools like OpenGL \cite{opengl} and Nvidia Cuda \cite{cuda} that are a good fit for tasks like these, these tools will also require some learning before I will be able to use them effectively. It is also important to identify what parts of the program can run on the GPU and which part can not, the more I am able to parallelize and run on the GPU the better and will give me a large performance gain. I also have to find out if I want to go for OpenGL or Nvidia's Cuda platform. This decision will be taken when I am closer to doing the actual implementation of the code. There are also libraries like PCL where I found the correspondence clustering algorithm, this library seems to have useful functionality when dealing with point cloud data, something I might likely work with.

\section{Conclusion}
This is going to be a large project and it will be interesting to see how far I can get within the time restrictions and to see if it is actually possible to achieve the goals I have set for the project. At this moment I have found several algorithms that seem promising and might be able to achieve what this projects requires. By testing more than one I also have other algorithms to fall back on if one does not perform well. I will also get some insight of which one is better suited for what kind of scenarios. It will be a very complex problem to solve and it has challenges that must be addressed as I encounter them. Even if the project is not successful, the knowledge that I acquire will be valuable for the continuation of this project. The field in which I will work in is underrepresented in the courses at UiO, so most of the knowledge I have to obtain myself in respect to how to use the platforms, handle 3D graphical data and if I have time, how to program with augmented reality in mind.

\newpage
\nocite{*}

\printbibliography

\end{document}
