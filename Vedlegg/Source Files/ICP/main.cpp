#include <pcl/registration/transformation_estimation_svd_scale.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/random_sample.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>

#include <iostream>

typedef pcl::PointXYZRGBA PointType;
typedef pcl::registration::TransformationEstimationSVDScale <PointType, PointType> TransEst;

pcl::PointCloud<PointType>::Ptr extract_sample(pcl::PointCloud<PointType>::Ptr const& cloud, int samplesize)
{
	pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>());
	std::vector<double> samples;
	samples.reserve(samplesize);

	pcl::RandomSample<PointType> random_sample;
	random_sample.setInputCloud(cloud);
	random_sample.setSample(samplesize);
	random_sample.filter(*cloud_filtered);
	
	return cloud_filtered;
}

pcl::PointCloud<PointType>::Ptr remove_outliers(pcl::PointCloud<PointType>::Ptr cloud)
{
	// Filter object.
	pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>());
	pcl::StatisticalOutlierRemoval<PointType> filter;
	filter.setInputCloud(cloud);
	filter.setMeanK(50);
	filter.setStddevMulThresh(1.0);
	filter.filter(*cloud_filtered);
	return cloud_filtered;
}


int iterations(200);

int main(int argc, char** argv)
{
	// Objects for storing the point clouds.
	pcl::PointCloud<PointType>::Ptr sourceCloud(new pcl::PointCloud<PointType>());
	pcl::PointCloud<PointType>::Ptr targetCloud(new pcl::PointCloud<PointType>());
	pcl::PointCloud<PointType>::Ptr finalCloud(new pcl::PointCloud<PointType>());

	// Read two PCD files from disk.
	if (pcl::io::loadPCDFile<PointType>(argv[1], *sourceCloud) != 0)
	{
		return -1;
	}
	if (pcl::io::loadPCDFile<PointType>(argv[2], *targetCloud) != 0)
	{
		return -1;
	}

	pcl::console::parse_argument(argc, argv, "--itr", iterations);

	//remove outliers
	//sourceCloud = remove_outliers(sourceCloud);
	//targetCloud = remove_outliers(targetCloud);


	// ICP object.
	pcl::IterativeClosestPoint<PointType, PointType> registration;
	registration.setInputSource(sourceCloud);
	registration.setInputTarget(targetCloud);
	registration.setMaximumIterations(iterations);

	registration.align(*finalCloud);
	if (registration.hasConverged())
	{
		std::cout << "ICP converged." << std::endl
				  << "The score is " << registration.getFitnessScore() << std::endl;
		std::cout << "Transformation matrix:" << std::endl;
		std::cout << registration.getFinalTransformation() << std::endl;
	}
	else std::cout << "ICP did not converge." << std::endl;

	pcl::visualization::PCLVisualizer viewer("Iterative Closest Point");

	pcl::visualization::PointCloudColorHandlerCustom<PointType> finalfilter_color(finalCloud, 0, 100, 255);
	viewer.addPointCloud(finalCloud, finalfilter_color, "source");

	pcl::visualization::PointCloudColorHandlerCustom<PointType> target_color(targetCloud, 255, 0, 0);
	viewer.addPointCloud(targetCloud, target_color, "target");

	while (!viewer.wasStopped())
	{
		viewer.spinOnce();
	}

	pcl::io::savePCDFileASCII("aligned.pcd", *finalCloud);

	return 0;
}