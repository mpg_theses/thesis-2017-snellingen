#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>


typedef pcl::PointNormal PointType;
typedef pcl::ReferenceFrame RFType;

std::string model_filename_;
std::string scene_filename_;

//Algorithm params
bool estimate_normals(true);
bool visualization(true);
bool compute_resolution(false);
bool rm_outliers(false);
bool white(false);
bool use_custom_viewpoint(false);
bool use_voxel_grid(false);
bool show_viewpoint(false);
bool use_normal_radius(false);
std::string output_file("output.pcd");

int k_value = 10;
float voxel_size = 0.25f;
float radius = 0.25f;

pcl::PointXYZ viewpoint;

pcl::PointCloud<PointType>::Ptr cloud;
pcl::KdTreeFLANN<PointType> kdtree;
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
void showHelp(char *filename)
{
	std::cout << std::endl;
	std::cout << "----- Normal - Estimate and show point cloud normals -----" << std::endl;
	std::cout << "Usage: " << filename << " pointcloud.pcd" << std::endl << std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "     -x:                     Do not do normal estimation (defualt false)" << std::endl;
	std::cout << "     -w:                     Show the point as white (default false)" << std::endl;
	std::cout << "     -q:                     Don't view the normals (default false)" << std::endl;
	std::cout << "    -cr:                     Compute the cloud resolution. (default false)" << std::endl;
	std::cout << "     -o:                     Remove outliers. (default false)" << std::endl;
	std::cout << "     -v:                     Downsample with voxel grid (default false)." << std::endl;
	std::cout << "    -nr:                     Use radius search for normal estimation (default false)." << std::endl;
	std::cout << "    -sv:                     Show viewpoint (default false)." << std::endl;
	std::cout << "     -h:                     Show help." << std::endl;
	std::cout << "     --k <integer>                   The k value for neighbors search in normal esitmation" << std::endl;
	std::cout << "     --r <float>                     Set radius. (default 0.25)" << std::endl;
	std::cout << "     --vs <float>                    Set voxel size (default 0.025)" << std::endl;
	std::cout << "     --cv <float>,<float>,<float>    Use a custom viewpoint" << std::endl;
	std::cout << "     --out <filename>:               Write the processed point cloud to a file." << std::endl;
}



void parseCommandLine(int argc, char *argv[])
{
	//Show help
	if (pcl::console::find_switch(argc, argv, "-h"))
	{
		showHelp(argv[0]);
		exit(0);
	}

	//Model & scene filenames
	std::vector<int> filenames;
	filenames = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");
	if (filenames.size() < 1)
	{
		std::cout << "Must have one point cloud file as input.\n";
		showHelp(argv[0]);
		exit(-1);
	}

	model_filename_ = argv[filenames[0]];
	pcl::console::parse_argument(argc, argv, "--out", output_file);

	std::string voxel_size_string;
	pcl::console::parse_argument(argc, argv, "--vs", voxel_size_string);
	if (!voxel_size_string.empty())
	{
		try
		{
			voxel_size = std::strtof(voxel_size_string.c_str(), 0);
		}
		catch (const std::exception&)
		{
			std::cout << "Could not parse voxel size, make sure it is a valid number. " << std::endl;
		}
	}

	std::string cvpr;
	pcl::console::parse_argument(argc, argv, "--cv", cvpr);
	pcl::console::parse_argument(argc, argv, "--r", radius);
	pcl::console::parse_argument(argc, argv, "--k", k_value);

	if (!cvpr.empty())
	{
		try
		{
			use_custom_viewpoint = true;
			std::string delimiter = ",";
			size_t pos = 0;
			std::string token;

			pos = cvpr.find(delimiter);
			token = cvpr.substr(0, pos);
			viewpoint.x = std::strtof(token.c_str(), 0);
			cvpr.erase(0, pos + delimiter.length());

			pos = cvpr.find(delimiter);
			token = cvpr.substr(0, pos);
			viewpoint.y = std::strtof(token.c_str(), 0);
			cvpr.erase(0, pos + delimiter.length());

			viewpoint.z = std::strtof(cvpr.c_str(), 0);

			std::cout << "Using [";
			std::cout << std::to_string(viewpoint.x) << ", ";
			std::cout << std::to_string(viewpoint.y) << ", ";
			std::cout << std::to_string(viewpoint.z);
			std::cout << "] as viewpoint" << std::endl;
		}
		catch (const std::exception&)
		{
			std::cout << "Unable to parse given viewpoint parameter: " << cvpr << std::endl;
			use_custom_viewpoint = false;
		}
		
	}


	if (pcl::console::find_switch(argc, argv, "-x")) {
		estimate_normals = false;
	}
	if (pcl::console::find_switch(argc, argv, "-nr")) {
		use_normal_radius = true;
	}
	if (pcl::console::find_switch(argc, argv, "-sv")) {
		show_viewpoint = true;
	}
	if (pcl::console::find_switch(argc, argv, "-v")) {
		use_voxel_grid = true;
	}
	if (pcl::console::find_switch(argc, argv, "-q")) {
		visualization = false;
	}
	if (pcl::console::find_switch(argc, argv, "-cr")) {
		compute_resolution = true;
	}
	if (pcl::console::find_switch(argc, argv, "-o")) {
		rm_outliers = true;
	}
	if (pcl::console::find_switch(argc, argv, "-w")) {
		white = true;
	}
}

double computeCloudResolution(const pcl::PointCloud<PointType>::ConstPtr &cloud)
{
	double res = 0.0;
	int n_points = 0;
	int nres;
	std::vector<int> indices(2);
	std::vector<float> sqr_distances(2);
	pcl::search::KdTree<PointType> tree;
	tree.setInputCloud(cloud);

	for (size_t i = 0; i < cloud->size(); ++i)
	{
		if (!pcl_isfinite((*cloud)[i].x))
		{
			continue;
		}
		//Considering the second neighbor since the first is the point itself.
		nres = tree.nearestKSearch(i, 2, indices, sqr_distances);
		if (nres == 2)
		{
			res += sqrt(sqr_distances[1]);
			++n_points;
		}
	}
	if (n_points != 0)
	{
		res /= n_points;
	}
	return res;
}

pcl::PointCloud<PointType>::Ptr remove_outliers(pcl::PointCloud<PointType>::Ptr cloud)
{
	// Filter object.
	pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>());
	pcl::StatisticalOutlierRemoval<PointType> filter;
	filter.setInputCloud(cloud);
	filter.setMeanK(50);
	filter.setStddevMulThresh(1.0);
	filter.filter(*cloud_filtered);
	return cloud_filtered;
}

int index = 0;
void pp_callback(const pcl::visualization::PointPickingEvent& event, void* viewer_void)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *>(viewer_void);
	std::cout << "Picking event active" << std::endl;
	if (event.getPointIndex() != -1)
	{
		float x, y, z;
		event.getPoint(x, y, z);
		
		PointType searchpoint = cloud->points[event.getPointIndex()];
		std::cout << x << ";" << y << ";" << z << std::endl;
		std::vector<int> pointIdxRadiusSearch;
		std::vector<float> pointRadiusSquaredDistance;
		pcl::PointCloud<PointType>::Ptr selected_cloud_cluster(new pcl::PointCloud<PointType>);

		if (kdtree.radiusSearch(searchpoint, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0)
		{
			for (size_t i = 0; i < pointIdxRadiusSearch.size(); ++i)
			{
				selected_cloud_cluster->points.push_back(cloud->points[pointIdxRadiusSearch[i]]);
			}

			selected_cloud_cluster->width = selected_cloud_cluster->points.size();
			selected_cloud_cluster->height = 1;
			selected_cloud_cluster->is_dense = true;
			auto name = "Selected" + std::to_string(index++);
			//viewer.addSphere(searchpoint, radius, "sphere" + std::to_string(index++));
			pcl::visualization::PointCloudColorHandlerCustom<PointType> color(selected_cloud_cluster, 255, 0, 0);
			viewer->addPointCloud<PointType>(selected_cloud_cluster, color, name);
			viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, name);
		}
	}
}

int main(int argc, char *argv[])
{

#if (!DEBUG)
	pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
#endif // RELEASE


	cloud.reset(new pcl::PointCloud<PointType>());

	//
	// Handle command line arguments
	//
	parseCommandLine(argc, argv);
	if (pcl::io::loadPCDFile(model_filename_, *cloud) < 0)
	{
		std::cout << "Error loading cloud." << std::endl;
		showHelp(argv[0]);
		return (-1);
	}

	//
	// Remove outliers
	//
	if (rm_outliers)
	{
		cloud = remove_outliers(cloud);
	}

	// remove outliers normals?


	//
	// Voxelgrid filter (downsample)
	//
	if (use_voxel_grid)
	{
		int original_size = cloud->size();
		//std::cout << "Cloud size: " << std::to_string(original_size) << std::endl;

		pcl::VoxelGrid<PointType> grid;
		grid.setInputCloud(cloud);
		grid.setLeafSize(voxel_size, voxel_size, voxel_size);
		// Temp variable used to avoid a bug that causes corruption if the input cloud also is used as output.
		pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>());
		grid.filter(*cloud_filtered);
		cloud = cloud_filtered;

		int current_size = cloud->size();
		//std::cout << "Cloud size after voxel: " << std::to_string(current_size) << std::endl;
		float reduction = (((float)original_size - (float)current_size) / (float)original_size) * 100.0f;
		//std::cout << std::to_string(reduction) << "% reduction" << std::endl;
	}

	// Used for radius search
	kdtree.setInputCloud(cloud);

	if (compute_resolution)
	{
		float resolution = static_cast<float> (computeCloudResolution(cloud));
		std::cout << "Model resolution:       " << resolution << std::endl;
	}

	//
	//  Compute Normals
	//
	if (estimate_normals) {
		pcl::NormalEstimationOMP<PointType, PointType> norm_est;

		if (use_normal_radius)
		{
			norm_est.setRadiusSearch(radius);
		}
		else
		{
			norm_est.setKSearch(k_value);
		}
		norm_est.setInputCloud(cloud);
		if (use_custom_viewpoint)
		{
			norm_est.setViewPoint(viewpoint.x, viewpoint.y, viewpoint.x);
		}
		norm_est.setNumberOfThreads(0);
		norm_est.compute(*cloud);
	}


	//
	//  Visualization
	//
	if (visualization)
	{
		viewer.reset(new pcl::visualization::PCLVisualizer("Cloud visualization"));
		viewer->registerPointPickingCallback(pp_callback, (void*)&viewer);
		viewer->setBackgroundColor(0, 0, 0);
		pcl::visualization::PointCloudColorHandlerCustom<PointType> single_color(cloud, 0, 255, 0);
		if (white)
		{
			viewer->addPointCloud<PointType>(cloud, "Point Cloud");
		}
		else
		{
			viewer->addPointCloud<PointType>(cloud, single_color, "Point Cloud");
		}
		viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "Point Cloud");
		viewer->addPointCloudNormals<PointType, PointType>(cloud, cloud, 10, 0.5, "Normals");

		if (show_viewpoint)
		{
			if (use_custom_viewpoint)
			{
				viewer->addCoordinateSystem(5.0, viewpoint.x, viewpoint.y, viewpoint.z, 0);
			}
			else
			{
				auto sensor = cloud->sensor_origin_;
				viewer->addCoordinateSystem(5.0, sensor.x(), sensor.y(), sensor.z(), 0);
			}
		}

		viewer->spin();
		//while (!viewer.wasStopped())
		//{
		//	viewer.spinOnce(100);
		//	boost::this_thread::sleep(boost::posix_time::microseconds(100000));
		//}
	}

	if (!output_file.empty())
	{
		std::cout << "Writing " << output_file << " with " << std::to_string(cloud->size()) << " points" << std::endl;
		pcl::io::savePCDFileASCII(output_file, *cloud);
	}

	return (0);
}