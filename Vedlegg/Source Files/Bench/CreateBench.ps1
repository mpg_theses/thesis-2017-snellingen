[CmdletBinding()]
Param(
  [Parameter(Mandatory=$True)]
   [string]$Model,
   [Parameter(Mandatory=$True)]
   [string]$Scene,
   [string]$Destination = "Benchmark",
   [string]$Preprocess = ".\preprocess_release.exe"
)

$count = 0;

New-Item $Destination -Type Directory -Force
New-Item "$Destination\scene" -Type Directory -Force
New-Item "$Destination\model" -Type Directory -Force


for ($voxel_size = 0.01; $voxel_size -lt 1.0; $voxel_size += 0.02)
{

    $count ++
    $voxel_size =[math]::Round($voxel_size, 4)
    $outpout_scene = "{0}\scene\scene_{1}.pcd" -f $Destination, $count
    $outpout_model = "{0}\model\model_{1}.pcd" -f $Destination, $count
    $command_model = "$Preprocess  $Model -x -q -v --vs $voxel_size --out $outpout_model"
    $command_scene = " $Preprocess  $Scene -x -q -v --vs $voxel_size --out  $outpout_scene"
    iex "&$command_model"
    iex "&$command_scene"
}

Write-Host "$count model scene pairs created"