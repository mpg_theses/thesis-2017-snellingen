Param(
   [string]$BenchmarkDirectory = "Benchmark",
   [string]$Correspondence = ".\corrgroup_silent.exe",
   [string]$Algorithm = "Hough",
   [int]$Cutdown = 0
)

$Models = Get-ChildItem Benchmark/model | sort-object {[int]($_.basename -replace '\D')}
$Scenes = Get-ChildItem Benchmark/scene | sort-object {[int]($_.basename -replace '\D')}

$n_samples = [math]::Min($Models.Length, $Scenes.Length)

$times = New-Object System.Collections.Generic.List[System.Object]
for ($i = $n_samples-1; $i -gt $cutdown; $i--)
{
    $model = $Models[$i].FullName
    $scene = $Scenes[$i].FullName
    $arguments = "-p -nk -q --algorithm $algorithm --rf_rad 1.5 --descr_rad 1.5 --cg_size 1"
    $command = "&$Correspondence $model $scene $arguments"
    $info = @{}
    $result = ""
    $info.time = (Measure-Command -Expression {$result = Invoke-Expression $command}).TotalMilliseconds
    $info.points = [int]$result
    $info.time
    $times.Add($info)
}

[void][Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms.DataVisualization")
# chart object
$chart1 = New-object System.Windows.Forms.DataVisualization.Charting.Chart
$chart1.Width = 1200
$chart1.Height = 600
$chart1.BackColor = [System.Drawing.Color]::White

# title
[void]$chart1.Titles.Add("Computational time Correspondence.exe")
$chart1.Titles[0].Font = "Arial,11pt"
$chart1.Titles[0].Alignment = "topLeft"

# legend
$legend = New-Object system.Windows.Forms.DataVisualization.Charting.Legend
$legend.name = "Legend1"
$chart1.Legends.Add($legend)

# chart area
$chartarea = New-Object System.Windows.Forms.DataVisualization.Charting.ChartArea
$chartarea.Name = "ChartArea1"
$chartarea.AxisY.Title = "Miliseconds"
$chartarea.AxisX.Title = "Number of points"
$chartarea.AxisY.Interval = 500
$chartarea.AxisX.Interval = 500
$chart1.ChartAreas.Add($chartarea)
$chart1.Series.Add('Time')
foreach ($datapoint in $Times) {
        $datapoint
        $chart1.Series["Time"].Points.addxy($datapoint.points, $datapoint.time)
    }
# data series
$chart1.Series["Time"].ChartType = "Line"
$chart1.Series["Time"].IsVisibleInLegend = $true
$chart1.Series["Time"].BorderWidth  = 3
$chart1.Series["Time"].chartarea = "ChartArea1"
$chart1.Series["Time"].Legend = "Legend1"
$chart1.Series["Time"].color = "red"


# save chart
$scriptpath = Split-Path -parent $MyInvocation.MyCommand.Definition
$chart1.SaveImage("$scriptpath\timetaken.png","png")