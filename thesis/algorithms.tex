\documentclass[document.tex]{subfiles}
\begin{document}

\chapter{The 3D Matching Pipeline} \label{pipeline}
There are several different algorithms with different purpose that have been used in this project. This chapter will explain how they work individually and how they are used in the bigger picture of achieving the goal of the thesis. The algorithms that are used must have some characteristics that makes them useful, they do not need to be perfect at all the mentioned points, but these are the things that were considered when selecting what algorithms to use: 
\begin{itemize}
\item Performance: The matching pipeline is intended to be able to run in a real-time environment that has limited performance and time. To start with, it is acceptable with slow performance of the pipeline if it has the potential for optimization that could in time make it fast enough at a later point. The fact that the matching will not be done with simple models but rather a complex environment with potentially high data density must also be taken into consideration.
\item Time: This goes hand in hand with performance as the algorithm must not require too much time to complete the task as this is going to be used in a real-time application. Because of this, the luxury of waiting a day or even 20 minutes for the computation to complete is not something that is practical. It is important how the algorithm's time complexity scales with the model complexity or if some of the work can be done in the preparation stage instead of the real-time environment. 
\item Space: Since the pipeline has is intended to be able to run on computer with limited resources, the 3D matching pipeline cannot use too much space as this is a limited resource. If the algorithm is robust enough, optimizations can be done on the data and what the algorithm process, such as only selecting the necessary amount of data to process.
\item Precision: The precision of the algorithm needs to be accurate enough where the user can make accurate comparisons between the virtual model and the real environment and not just a very rough estimate with the alignment result from the pipeline. 
\item Complexity: The pipeline should not be too complex, as the project has a limited time frame for implementing and testing the algorithms. They should also preferably not rely on a complicated setup or infrastructure for it to work as intended.
\item Robustness: Since the collected data of a real environment can have certain problematic artifacts like occlusion and missing information, the pipeline should be able to handle to these issues. Robustness is important because there is no way guarantee a fully complete and error free 3D representation of the environment to perform 3D matching on.
\end{itemize}

To recognize objects and features in a 3D point clouds, a common approach is to combine several steps together into a pipeline. Such a pipeline can look like the one proposed in \citetitle{hough} \cite{hough}. A general high-level overview of such a pipeline would contain components such as keypoint extraction, feature description, matching, alignment and refinement. All these components are not necessary in every scenario but give a general description of a typical 3D matching pipeline.


\begin{figure}[htpb]
\centering
\includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/3dmatchingpipeline2}
\caption{The high-level illustration of a generic 3D matching pipeline and its components.}
\label{fig:3Dpipeline}
\end{figure}

\begin{enumerate}

\item \textbf{Keypoint Extraction:}
This is a component where the focus is to select a subset of points that should be used instead of an exhaustive selection that contains every single point in the cloud. Keypoint Extraction can be as simple as selecting a uniform random sample of points in the cloud. There are also techniques that selects optimal points in the cloud that contains the most relevant features of the cloud and others that focus on retaining as much information as possible. This step is usually at the beginning of the pipeline so that the rest of the components only need to work with the selected keypoints. 

\item \textbf{Feature Description:}
This component has the purpose of giving a description of a point that can be used when comparing with other points. This is usually done by analyzing the neighborhood of the point and store this in a histogram or a signature. There exists several different approached for creating a feature description, a few examples are highlighted in chapter \ref{descriptors}. These histogram or signatures will give a description of the points and its close neighborhood that can be used for comparing points with each other.

\item \textbf{Matching:}
Matching is where the point-to-point matches, or similarities in the clouds are found between the two clouds. This is done by measuring the distance or similarity between the descriptions of the points. The selected points that fall within a certain acceptable threshold are stored as correspondences. 

\item \textbf{Correspondence Grouping:}
Correspondence grouping is a technique that focusing on finding correspondences that are more likely to be correct. The grouping method tries to consider a larger section of an area instead of a single matched point. This is useful as a single point can match with a similar feature in a wrong location, but when a larger section and group of correspondences is considered, single false positives can be filtered out.

\item \textbf{Alignment:}
This is the component that takes care of the pose estimation (estimating the alignment) of the model. The purpose here is to calculate an alignment that best fits the matched correspondences found from the matching step or correspondence grouping step.

\item \textbf{Refinement:}
When the alignment is not perfect or needs some adjustments, the purpose of the refinement component is to perform the final fine-tuning of the alignment. This is an important step when the alignment needs to be as precise as possible and the rough estimation is not good enough. 

\end{enumerate}

\section{Keypoint Exctraction} \label{Keypoint Extraction} 
The point of the keypoint extraction is to reduce the amount of point necessary to work with. The reason for this is that a point cloud can consist of large amount of points and processing all of them can be computationally heavy and exhaustive. By reducing the point down to a selected amount of keypoints, the 3D matching pipeline can perform the same functionality with fewer points, which saves time and computational power. The keypoints selected can be an optimal selection of points based on the clouds features or it can be as simple as selecting a random subset of points. A sophisticated approach of keypoint selection could be Intrinsic Shape Signatures (ISS) proposed by \citeauthor{ISS} in \citetitle{ISS} \cite{ISS}. This ISS key point selection is a little overkill for the 3D matching pipeline in this project, but it could be considered in a more optimized version. As the keypoint selection step is not the most essential step beside performance improvement, it is not necessary for this project to implement the most advance selection method available to achieve the desired matching results. The Keypoint Extraction method for this project should not cause a major loss in information while also be able to down sample a cloud considerably, an algorithm that does this is the Voxel Grid Filter. 

\subsection {Voxel Grid filter}\label{voxel}
% Where does this algorithm come from? Find the source and cite it.
Voxel Grid filter is often used to downsample point clouds to a lower resolution or to normalize the point distribution in the cloud. When it comes to downsampling, the Voxel Grid Filter can reduce the points needed while still retaining the as much of the original information as possible. The voxel grid approach does not simply return a lower subset of points like random uniform selection, the Voxel Grid filter returns a new point cloud which should represent the input cloud as close as possible given the set sampling size. This also has the effect of creating a more even distribution of points as the size of the voxels is the same across the whole cloud.
\begin{figure}[htpb]
\centering
\includegraphics[width=\textwidth,height=5cm,keepaspectratio=true]{img/voxelgrid}
\caption{A 3D grid over a point cloud where each green box is a voxel. \cite{imgvoxel2}}
\label{fig:voxel grid}
\end{figure}
\noindent The voxel grid filter works by taking a spatial average of the cloud. In the case of a 2D cloud, it would simply divide it into a regular grid of rectangles, for the 3D space, these rectangles become boxes as illustrated in figure \ref{fig:voxel grid}. These rectangles, or boxes in the 3D context, are called voxels. The next step is to select a single point inside this voxel, this can be done by either just selecting the geometric center of the voxel or by selecting the centroid point inside the voxel. Selecting the centroid will give better results as it takes into consideration the points inside the voxel, but this approach is more computationally heavy as this needs to be computed for each voxel. The computational cost increases linearly with the number of points in the cloud and the number of voxels. The sample size of this filter is set simply by changing the size of the voxels, where smaller voxels will give a greater sample size.

\begin{figure}[htpb]
\centering
\includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/voxel}
\caption{Illustration of a cloud that has been downsampled with voxel grid sampling. \cite{imgvoxel}}
\label{fig:voxel grid filter illustration}
\end{figure}

\section{Feature Description}\label{descriptors}
Descriptors are complex and precise signatures of a point that can be used to identify a point across multiple point clouds. To be able to match a 3D point cloud with another, the algorithm needs to be able to find certain features from the cloud that it can use for matching. These features must fulfill some criteria to be useful as mentioned in \citetitle{descriptors} by \citeauthor{descriptors}\cite{descriptors}: 
\begin{itemize}
\item Robust to transformations: Transformations that does not change the distance between points should not affect the feature. So even if the cloud is translated or rotated, it should still have the same features. 
\item Robust to noise: Scanned clouds will have some errors that can cause some noise, the features should not be affected too much by this noise.
\item Resolution invariant: Having different density on the cloud should not greatly impact the features.
\end{itemize}

\noindent Descriptors can be categorized into two main categories, these are global descriptors and local descriptors.

\begin{itemize}
\item Global descriptors: These descriptors encode the object geometry from the whole cluster that represent that object. These descriptors are used for object recognition, classification and geometric analysis \cite{globaldesc}.

\item Local descriptors: These descriptors only describe the local geometry around a point and alone have no concept of the whole object. These descriptors do not govern what kind of keypoints selection that some global descriptors do. Because of this, a simple keypoint selection by selecting random points is sometimes good enough \cite{localdesc}. The focus of this project is on local descriptor as these are better fitted for the kind of matching we are trying to perform as we are not interested in description the environment as a whole.
\end{itemize}

\subsection{Point Feature Histogram PFH}
% TODO: FIND ORIGINAL SOURCE.
PFH is a local descriptor, so it calculates its descriptor based on single points and their nearest neighbors. This descriptor was first proposed in \citetitle{pfhorigin} by \citeauthor{pfhorigin} in \citeyear{pfhorigin} \cite{pfhorigin}. This algorithm gives a set of key points that will process and create a description for. The goal of PFH is to compute and analyze the difference between the normals from the neighbor points of the selected keypoint. To do this, the algorithms pairs all the neighboring points within a certain radius. For each of these points, a fixed coordinate frame is computed from their normals. Equations from \citetitle{pfh} \cite{pfh}:

\begin{equation}
\begin{aligned}
u &= n_s \\
v &= u \times \frac{(p_t - p_s)}{\left \| p_t - p_s \right \|_2} \\
w &= u \times v 
\end{aligned}
\end{equation}
Using this $uvw$ frame, the difference between the two normals $n_s$ and $n_t$ can be calculated like this:
\begin{equation}
\begin{aligned}
\alpha &= v \cdot n_t \\
\phi  &= u \cdot \frac{(p_t - p_s)}{d} \\
\theta &= arctan(w \cdot n_t, u \cdot n_t)
\end{aligned}
\end{equation}
$\alpha, \phi, \theta$ is then saved together with the Euclidean distance and gets binned into a histogram for each saved value. The resulting histograms are then concatenated together to form a descriptor for this keypoint that describes the local neighborhood and their estimated surface normals. 
\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/pfh_diagram}
    \caption{
        Illustration for the PFH descriptor where it computes the relationships between all pairs of points within the radius of the point (red)\cite{pfh}.
    }
    \label{fig:PFHneighbors}
\end{figure}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/pfh_frame}
    \caption{
        Illustration of the $uvw$ used to calculate the difference between normals. \cite{pfh}.
    }
    \label{fig:PFHuvw}
\end{figure}

\noindent PFH has good accuracy and it copes well with different sample densities, noise levels and is invariant to rotation and translation. An important note is that the algorithm is very dependent on the quality of the point normals for giving good results. This computational complexity of the algorithm is $O(nk^2)$ where $k$ is the number of neighbors for each point $n$ in the cloud. Because of the large computational complexity of this algorithm, it is not feasible to use in real-time applications \cite{pfh}
\cite{descriptors}.


\subsection {Fast Point Feature Histogram (FPFH)} \label{fpfh}
%% TODO FIND ORIGINAL SOURCE
Fast Point Feature Histogram is a simplification of Point Feature Histogram proposed by \citeauthor{fpfhorigin} in \citetitle{fpfhorigin} \cite{fpfhorigin}. The purpose of the algorithm is to achieve a similar result with smaller computational complexity so that is feasible to use in real-time applications.  FPFH considers only the direct connections between the key point and its neighbors, not like PFH which considers all the connections. FPFH cannot simply forget about those missed connection and expect a close or identical result as PFH, therefore FPFH add an additional step to the algorithm to account for the loss of these extra connections.

FPFH calculates the same set of tuples $\alpha, \phi, \theta$ between itself and its neighbors the same way as described in PFH, this result is called Simple Point Feature Histogram (SPFH) \cite{fpfh}. In the newly added second step, each points $k$ neighbors are re-determent and their SPFH values are used to weight the final histogram from the first step. This performed with this formula \cite{fpfh}:

\begin{equation}
\begin{aligned}
FPFH(p_q) = SPFH(p_q) + \frac{1}{k} \sum_{i=1}^k \frac{1}{w_k} * SPFH(p_k)
\end{aligned}
\end{equation}

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/FPFH_diagram}
    \caption{
        Example on what points pairs are computed for the FPFH descriptor \cite{fpfh}.
    }
    \label{fig:FPFHneighbors}
\end{figure}

\noindent The algorithm has a computational complexity of $O(nk)$ where $n$ is key points and $k$ is neighbors. Because of the last step that is added to this algorithm, the accuracy and reliability of the algorithm does not decrease significantly. This makes this algorithm feasible to use for real-time applications. 

\subsection {Signatures of Histograms of Orientations (SHOT)} \label{shot}
Signature of Histogram of Orientation, dubbed SHOT is a rotation and transformation invariant descriptor invented by \citeauthor{shot1} found in the paper \citetitle{shot1} \cite{shot1}. The descriptor encodes information about the surrounding topology that resides inside its spherical support structure and use both histogram and signatures to do this. SHOT also use a unique and robust local reference points to keep the descriptor rotation and transformation invariant.
\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/shotsphere}
    \caption{
        Illustration of the division of the spherical support structure. For simplicity sake, only 4 azimuth divisions hare shown \cite{shot1}.
    }
    \label{fig:SHOTsupport}
\end{figure}
The descriptor enhances itself by introducing a signature trait by recording information about the location of the points within the surface of the neighboring points. The histograms are created by superimposing a 3D grid over the 3D surface of the neighboring points. The local histograms are created by binning the points based on a function that takes all the angles between the normals of the points found inside the grid and the normals for each point. The structure of the signature uses an isotropic spherical grid. The grid is obtained by divided the sphere into several spatial bins. Experimentation from the paper \citetitle{shot1} by \citeauthor{shot1} \cite{shot1} indicated that 32 spatial bins are proper number of bins. These are obtained by splitting the sphere up with 8 azimuth division, 2 elevation division and 2 radial divisions. For each point accumulated into a local histogram bin, a quadrilinear interpolation with its neighbors are performed to avoid boundary effects that might arise. Each count into the bin is multiplied with a weight of $1-d$ for each dimension:
\begin{itemize}
\item Local histograms dimension: $d = $ the distance between the point and the central value of the bin.
\item Elevation dimension: $d = $ the angular distance of the point and the central value of the volume.
\item Azimuth dimension: $d = $ the angular distance of the point and the central value of the volume.
\item Radial dimension: $d = $ the Euclidean distance between the point and the central value of the volume. 
\end{itemize}
For each of the dimension, $d$ is normalized by the distance of its two neighboring bins or volumes. The descriptor is also normalized to sum up to 1, t normalization is performed to keep the descriptor more robust against different point cloud densities.

\section{Matching} \label{matching}
The matching step has the purpose to find all the point-to-point matches between the two point clouds. Because the descriptions of the points have been calculated, these are used to measure the similarities or distance between the points in the cloud. The search for these points can be done the exhaustive way by matching every point in cloud A with every point in cloud B, but this is very computationally heavy. A more optimized approach would be to use a k-d tree structure (explained in chapter \ref{kdtree}) to help with the search mentioned in \citetitle{pclcorrespondence} \cite{pclcorrespondence}. A k-d tree is created from the cloud A descriptors and for each descriptor from cloud B, it finds the most similar descriptor based on the Euclidean distance. If the distance or similarity of the point is within a given threshold, the point is stored as a correspondence.

\section{Correspondence Grouping}\label{cg}
Correspondence grouping is a technique that focusing on finding the matched correspondences that are most likely to be correct. The reason why correspondence grouping is attractive for a general 3D matching pipeline is that it filters out the correspondences that do not really give any value or are incorrect, making the remaining result more accurate and useful for alignment. Correspondence grouping is also not limited to one object alone, but it can also recognize multiple objects at the same time. This algorithm flexible enough to work with aligning virtual environment over another environment and not simply recognizing small distinct objects in a scene. The correspondence grouping technique not very computationally heavy and is tolerant against common artifacts associated with 3D points clouds. Because of these attributes, this technique is attractive to use in a 3D matching pipeline. There are two main approaches available in the Point Cloud Library (chapter \ref{pcl}) and those are: Hough3D \cite{hough} and Geometric Consistency \cite{gc}.

\subsection{Hough3D Grouping} \label{hough}
Hough3D grouping is based on a 3D Hough voting scheme found in \citetitle{hough} by \citeauthor{hough} \cite{hough}. The goal of the Hough3D grouping algorithm is to accumulate evidence by voting on the possible presence of the search object in the scene and the objects pose is determined based on the computed correspondences. 

\subsubsection{How it works}
The 3D Hough transform in \citetitle{hough} proposes an approach for detecting free-form shapes in a 3D space that is also able to be done from real-time data. The algorithm is performed on two point clouds, a model cloud and a scene cloud. The model cloud is the search object the algorithm is trying to find in the scene cloud. 
\begin{figure}[htpb]
\centering
\includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/houghvoting}
\caption{Illustration of correspondence matching in 3D Hough Voting. \cite{hough}}
\label{fig:corrillustration}
\end{figure}
From the set of feature descriptors from both the object and scene point cloud, a set of correspondences is determined by thresholding. The threshold can be the Euclidean distance between the descriptors, illustrated as green lines in figure \ref{fig:corrillustration}. The correspondence set will have some wrong correspondences. (illustrated as red arrow in figure\ref{fig:corrillustration}). The reason for this can be different factors like noise, occlusion and clutter in the point clouds. A unique reference point $C^M$ is computed for the model, this is illustrated as a red circle in figure \ref{fig:corrillustration}. The method for this computation is not important as it has no effect on the performance of the algorithm. With this reference point, a vector between each feature ${F^M}_i$ and the  $C^M$ point is created, illustrated by the blue arrows in figure \ref{fig:corrillustration}. To keep the algorithm invariant to rotation and translation, it is needed to compute an invariant reference frame for each keypoint from both the model and the scene cloud, these reference points needs to be efficiently computed as it needs be computed for every keypoint. 

\begin{figure}[htpb]
\centering
\includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/hough_rf}
\caption{Example of the transformation between the reference frames. \cite{hough}}
\label{fig:refillustration}
\end{figure}
\noindent Once all the correspondences have been found, the voting scheme can start. For every feature in the scene where there was a matching correspondence with the object, a vote is cast for the reference point $C_S$ position in that scene. When enough features have voted the presence the search object, the search object has been found and it can be aligned with the established correspondences.
\begin{figure}[htpb]
\centering
\includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/houghvoting2}
\caption{Example of the vote casting done in the 3DHough algorithm.  \cite{hough}}
\label{fig:voting}
\end{figure}

\subsubsection{Origins of the algorithm}
The algorithms are based on successive derivations of the original Hough Transform algorithm found in the patent \citetitle{houghpatent} by \citeauthor{houghpatent} posted in \citeyear{houghpatent} \cite{houghpatent}. However, the Hough transform that is universally used today for 2D spaces is the Generalized Hough Transform (GHT) invented by \citeauthor{ght} in \citeyear{ght} \cite{ght}. The Generalized Hough Transform algorithm removed a major limitation of the original Hough Transform. The limitation with Hough Transform is that every vote is accumulated in an accumulator that has the same number of dimensions as the number of unknown parameters considered for the shape class. This has the side effect of restricting the algorithm from being applied to shapes that have too many parameters as the votes will cause a sparse, high-dimensional accumulator. This is not the case for Generalized Hough Transform that extends the algorithm to support any arbitrary shape. This is done by having each feature voting for a specific position, orientation and scale factor for the searched shape. The extension of the original Hough Transform algorithm to the 3D space allows for detection of planes within 3D point clouds and smaller analytical shapes like spheres and cylinders. There also exists a proposed extension of the Generalized Hough Transform to the 3D space, but it is computationally heavy as it deals with a 6-dimensional space Hough space.

\subsection{Geometric Consistency Grouping}\label{gc}
This is an alternative and less complex approach for doing correspondence grouping that can be used instead of the Hough3D Grouping algorithm. This approach is based on the proposed solution in \citetitle{gc} by \citeauthor{gc}.\cite{gc} This algorithm works in a similar way as the Hough3D, it need the same correspondences, but it does not need to calculate a local reference frame for all the descriptors. Information such as model ID, feature histogram, surface type and the reference points are saved into a hash table.
\begin{figure}[htpb]
\centering
\includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/hastable}
\caption{The structure of the hastable.  \cite{gc}}
\label{fig:hastable}
\end{figure}
A vote is cast to the hastable if the differences between feature histograms falls under a certain threshold and the geometric surface type is consistent. The threshold can be the Euclidean distance between the reference points. After the voting, all hash table entries are binned into a histogram and fetches the models that get the top three highest votes. 

\subsection{Alignment based on correspondences}\label{alignment}
Alignment of the cloud or pose estimation that it is often called, is to compute the rotation and translation that best fits the correspondences found. This is something that can be obtained from the correspondence grouping as they group the correspondences and find the established correspondences where the pose of the model can be extracted. The method used in by the algorithm is the absolute orientation found in the article  \citetitle{absorient} by \citeauthor{absorient} \cite{absorient} that was mentioned in \citetitle{hough} \cite{hough}.

%\subsection{Random Sample Consensus}

\section{Refinement}
The refinement part of the pipeline is necessary when the alignment at this point is not completely correct and needs small adjustments. The alignment from the matched cloud and correspondence grouping will likely not be far from the correct position, it therefore enough to do incremental small adjustments until the desired alignment is achieved. For accomplishing this task, the simple and well known Iterative Closest Point algorithm is good enough to complete the job. Iterative Closest Point is not the only algorithm that can perform refinement on the alignment, but it is a simple and well-known implementation that is able to perform such small alignment corrections without getting too complicated. Another example of a possible algorithm that could be used here instead of Iterative Closest Point is the Random Sample Consensus (RANSAC) algorithm that was published by \citeauthor{ransac} in \citetitle{ransac} \cite{ransac}. The RANSAC algorithm is not explored in this project.
 
\subsection{Iterative Closest Point} \label{icp}
Iterative closes point is a well-known algorithm introduced by \citeauthor{icp1} in \citetitle{icp1} \cite{icp1} and in article \citetitle{icp2} by \citeauthor{icp2} in \citeyear{icp2} \cite{icp2}. The purpose of the algorithm is to minimize the error in alignment between to point clouds. It is useful to fine tune estimated alignments and matching on a point cloud so that you achieve a more precise result. It is often used as registrations technique where the goal is to align two point clouds with a slight shift or difference with each other so that they can be stitched together. If you have two point clouds, a source cloud and reference cloud, the algorithm would work like this:
\begin{enumerate}

\item For each point the source cloud matches the closest point in the reference point cloud set. By this, it finds the closes point in the same location in the reference cloud and matches these two together.
\item Calculate an error metric between the clouds,  this can be the sum of squared distances between the matched points. 
\item Adjust the transformation of the cloud and minimize the point-to-point distance between all the matched points based on the error metric. 
\item Iterate until converged or max iterations have been reached.
\end{enumerate}
Iterative Closes Point does not perform well on clouds that have their initial alignment too far from each other or have different scaling. Therefore this algorithm is mostly useful when the clouds only need some adjustments to their initial alignment.

\section{Global Hypothesis Verification} \label{hypothesis}
This is an additional step that can be used at the very end of the matching pipeline. If several different results are given on where the sane matched object is located on the scene, the Hypothesis Verification procedure can help with selecting of the best fitting result. The Global Hypothesis algorithm used in the Point Cloud Library is based on method found in \citetitle{hypothesis} by \citeauthor{hypothesis} \cite{hypothesis}. The algorithm tries to remove false positive results by determining the object and pose instances given. The algorithm uses a global optimization stage based on a cost function that includes geometric clues. This method has the advantage of being able to detect objects in significantly occluded objects without increasing the number of false positives.

% Determin object and pose instances according to a global optimization stage based on a cost function which emcompasses geometric cues.
% Has the ability to detect signifiacantly occluded objects without increasing the amount of false positvies. 

\section{Supporting Algorithms}

\subsection{K-Dimensional Tree}\label{kdtree}
A k-dimensional tree (k-d tree) is a data structure for organizing point into a k-dimensional space invented by \citeauthor{kdtreeorigin} in the published article \citetitle{kdtreeorigin} in \citeyear{kdtreeorigin} \cite{kdtreeorigin}. Several different descriptor algorithms use a k-d tree structure to help with the search for nearest neighbor or radius search for points. Essentially a k-d tree is a binary tree where every node is a k-dimensional point. Since we are only going to deal with 3-dimensional point clouds, our k-d trees are also going to be 3-dimensional. Every level except the leaf nodes generates a splitting hyper-plane, dividing the space into two parts. A hyper-plane is a plane along a specific dimension, in the case of a 3-dimensional k-d tree, these hyper-planes will be 2-dimensional surfaces. At the root, or the first node of the tree, the split is based on the first dimension. This mean that at the top, all the children nodes will be split based on if its first dimensional coordinate is less or greater than first dimensional coordinate of the root node. Each level down the tree divides on the coordinate in the next dimension and returning to the first dimension when the max dimension is reached. \cite{kdtreewiki} \cite{kdtreepcl}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/kdtree_2D}
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/kdtree_3D}
    \caption{
        Illustration of a 2-dimensional (left) and a 3-dimensional k-d tree (right). \cite{kdtreewiki}.
    }
    \label{fig:k-d tree illustration}
\end{figure}

\subsubsection{Nearest-Neighbor Search}\label{kdsearch}
A useful and common use of a k-d tree is find the nearest neighbor or neighbors of a given point. This is a problem solved by k-d tree for many different local descriptor algorithms when analyzing the points in the vicinity. The algorithm for finding nearest neighbor works like this when using a k-d tree: 

\begin{enumerate}
\item Start from the root node (the node at the top of the tree) and move down the tree recursively. While traversing the tree, move either left or right depending on whether on the point being searched on is on the left or the right side of the splitting plane. This is the same method used when executing and insert of a new element in the tree. 
\item When the algorithm reaches a leaf node (an end node at the bottom), store that node as the ''current best''.
\item Move back up the three by unwinding the recursive path. For each of the nodes back up the tree, check if the node closer to the splitting hyper-plane than the stored ''current best''. If the node is closer, replace the ''current best'' with the new node. The algorithm also checks for other potential points on the other side of the splitting plane. This is done by checking whether distance between the splitting coordinate of the search point and the current node is lesser than the distance from the search point to the ''current best''.
\item The algorithm is finished when it reaches and finished processing the root node.
\end{enumerate}
\cite{kdtreewiki}

% TODO mention time complexity?

\subsection{Normal Estimation}\label{normalestimation}
A surface normal is a vector that is perpendicular to a given surface. This property is very important and useful for many different algorithms that works with 3D and geometric data. In our case, many of the different descriptor algorithms uses surface normals as a core part of their algorithm. Because of this essential role, it is important to have a good surface estimation for the data set. Having a bad surface estimation can greatly impact the performance and quality of the different matching algorithms that heavily dependent on it. Normally a single point in a point cloud does not contain the necessary information for calculating the surface normal for that given point. Estimating the surface normal need to be done with information from the surrounding points. This can be done in two main ways as mentioned in \citetitle{pclnormal} \cite{pclnormal}:
\begin{enumerate}
\item Using surface meshing techniques to create a surface mesh which can be used to calculate the surface normals from as a mesh has the necessary information required.
\item By using approximation to guess the underlying surface of the points directly so that we can use that for the normal estimation.
\end{enumerate}

The surface estimation method used by the point cloud library and this project is based on the second approach. The method is to directly compute the surface normals of each point in the cloud. This is done by reducing the problem of estimating the normal to a problem of finding the normal of plane that is tangent to the surface of the surrounding points. The algorithm calculates the covariance matrix $C$ based on k number of neighbors around a selected point $p_i$, the centroid f$\bar{p}$ for these points is also found. Covariance is the measure of how much from the mean that two random variables vary from each other. Essentially it computes the linear relationship between the points in this neighborhood. This is gathered in a 3x3 covariance matrix and is created with this formula \cite{pclnormal}:

\begin{equation}
\begin{aligned}
\mathcal{C} = \frac{1}{k}\sum_{i=1}^{k}{\cdot (\boldsymbol{p}_i-\overline{\boldsymbol{p}})\cdot(\boldsymbol{p}_i-\overline{\boldsymbol{p}})^{T}}, ~\mathcal{C} \cdot \vec{{\mathsf v}_j} = \lambda_j \cdot \vec{{\mathsf v}_j},~ j \in \{0, 1, 2\}
\end{aligned}
\end{equation}

Then a Principal Component Analysis (PCA) is done on the eigenvectors $\vec{\mathsf v}$ and eigenvalues $\lambda$ of the covariance matrix $C$. Principal Component Analysis was invented by \citeauthor{pca} in \citeyear{pca} found in \citetitle{pca} \cite{pca}. A Principal Component Analysis creates a new coordinate system for the points where points with the largest deviations lies on the first axis and the second greatest on the third axis and so on. This will give us an orientation related to the surface surrounding our selected point.
\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/pca}
    \caption{
        Illustration the first two component (first two axis) given from a PCA analysis on a set of points. \cite{pcakevin}.
    }
    \label{fig:k-d pca component illustration}
\end{figure}
\noindent The orientation of the normal for this surface is still ambiguous as there is no way to know the sign of the normal, but because we know of the viewpoint $v_p$, it is possible to orient the normals $\overrightarrow{n_i}$  consistently across the point cloud by making sure they satisfy this equation:
\begin{equation} \label{eq:viewpoint}
\begin{aligned}
\vec{\boldsymbol{n}}_i \cdot ({\mathsf v}_p - \boldsymbol{p}_i) > 0
\end{aligned}
\end{equation}
\cite{pclnormal}

\subsection{Shape Distribution} \label{shapedist}
Shape distribution is an algorithm found in \citetitle{shapedist} by \citeauthor{shapedist} \cite{shapedist}. The algorithm creates a global signature of a model point cloud and creates a shape distribution by histogram binning. An example of how a Shape Distribution can look can be seen in figure \ref{fig:shapedist}. There is different method of creating a signature of the model, a simple yet effective one is to sample random points in the cloud and measure the distance between them. Creating such a shape signature is $S\log{N}$ where $S$ are samples for a shape function that measure the distance between two random points on the surface, and $N$ is the number of points. When you have the shape distribution, you can use functions that have a constant complexity and comparison cost. The algorithm claim that it is robust because of its random sampling and will cause this algorithm to be insensitive to small perturbations \cite{shapedist}. However, because of the lack data to use for alignment with this kind of signature, it is not useful for our project for other than giving an indication that the search is similar to the full environment scene. 

\end{document}