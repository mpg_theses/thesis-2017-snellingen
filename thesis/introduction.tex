\documentclass[document.tex]{subfiles}
\begin{document}

\chapter{Introduction}

% mention the increase of 3D sensor and the rise of virtual reality and mixed reality.
% demand for overlaying virutal scenes and objects over real world objects and scenes.

DNV-GL is a certification body and classification society and performs technical inspections and validation, for example, of a vessel under construction. When inspecting a construction, the inspector needs to find the correct drawings to write down what must be corrected and run around and take precise measurements of the environment. This process is time-consuming and requires a fair amount of preparation before the inspection can start. By using mixed reality and matching the construction to be inspected with a 3D representation and tracking, the inspector can move around freely in both the real and virtual representation of the construction. Here the inspector can annotate the corrections and improvements by simply pointing/gesture towards what he sees and add the note. This requires tracking and 3D reconstruction of the real environment and a good 3D matching algorithm. The focus of this thesis is on the 3D matching algorithm that can be used to align a virtual 3D object. 

\section{The problem to solve}
One major steppingstone to achieve a mixed reality function as described above is to be able to perform reliable 3D matching and alignment. This makes it possible to overlay a virtual 3D model over the scanned environment. There are several problems that need to be accounted for, such as occlusion, clutter and artefact caused by real time 3D scanners. A way to match a 3D model with the scanned data also need to be performed and will present problems with how they fundamentally differ from each other. A 3D model contains more information that a scan will provide. The matching pipeline proposed in this thesis will be able perform a match and alignment with a 3D scan of an environment with a 3D model representation of the same environment.

\section{Goal}
The goal is to find and implement different algorithms and techniques that can achieve 3D matching of a reconstructed or scanned 3D environment with a virtual 3D model. The focus will be to find such algorithms that are able to help with accomplishing this task with high enough accuracy to be useful. The algorithms should also not be too computationally heavy so that it can be refined to work on a mobile device in the future. To start with, it is enough for the algorithm to be able to run on a computer that can fit into a car and be transported. It is important to get to know each step required to achieve 3D matching well enough to understand its limitations and how it can be adjusted and applied to our problem.

%\section{Research question}

\section{Organization}
This thesis is divided into several chapters that focus on certain parts of the project. The main chapters can be divided into these categories: 

\begin{enumerate}
\item \textbf{Introduction}: the purpose of this chapter is to give the reader an understanding of what the thesis is about and how the thesis is structured.  
\item \textbf{Background}: This chapter explains some background information about the subject of the thesis.
\item \textbf{The 3D Matching pipeline}: Here all the different algorithms used and are critical for the performance for achieving goal of the thesis are presented. There are many different algorithms and techniques that together depend on each other for creating a working 3D matching pipeline. It is also important to understand the algorithms and how they work for identifying possible problems and limitations.
\item \textbf{Design and Implementation}: This part describes the different utilities created under this project and how they were implemented.
\item \textbf{Software, Libraries and Tools}: This chapter presents all the different tools that were used under the project to help with implementation, creation of test data and illustration.
\item \textbf{Testing and Evaluation}: This chapter has the purpose of evaluating the testing results done with the tools created under this project.
\item \textbf{Conclusion}: This part concludes the thesis and its results. It also contains some discussion on what could be done differently and what can be done in the future.
\end{enumerate}

\section{Background}
There is a larger need for technology to understand its environment and give helpful information. One of the large barriers to overcome is to make software understand the space it is working in. Even if you have a good 3D representation of the environment, it is not straight forward to extract relevant information and use it for your intended purpose. For this project, the need to understand the shape and the environment is important so that it can be used to align a virtual representation over it. By using 3D scanning technology more information about the environment is collected than what a normal 2D image can provide, but this also gives more data that need to be handles and analyzed. This is where the 3D matching algorithms come into the picture. There are many different algorithms with different purposes when it comes to recognizing and analyzing the 3D spaces. Many of these are hand crafted for a single purpose, such as classification, where they only group the 3D model or scene into a class. Recognizing that a 3D representation of a table is an actual table does not necessarily give us enough useful information. The problem with this kind of result is that you usually end up with a statistical representation of the object, like a histogram, that describe the object's characteristics. An example of such an algorithm is the Shape Distribution (see chapter \ref{shapedist}) found in \citetitle{shapedist} by \citeauthor{shapedist} \cite{shapedist}, and is one of the corner stone algorithms that many other build upon. The algorithms analyzed a set amount of points of the objects and bins these results into a histogram that represents the feature characteristics of the model. 
\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth,height=5cm,keepaspectratio=true]{img/ABbunny}
    \includegraphics[width=\textwidth,height=8cm,keepaspectratio=true]{img/shapedistbunny}
    \caption{
        Shape Distribution histogram of a full rabbit A (left) and a scanned rabbit B (right).
    }
    \label{fig:shapedist}
\end{figure}
However, if we want to match a scan that only has data about a segment of the full 3D representation of the object, the information given from classification will likely not match. The small segment does not contain more information than collected while the full 3D representation has a complete picture, thus creating a difference in their characteristics. This problem is illustrated in figure \ref{fig:shapedist} where a scanned 3D representation of a bunny gives a different result than the full 3D representation. Even if these were to match, there is still the issue of aligning them correctly. Even if the results from the classification algorithms do not fit this project requirements, they still perform basic analytical steps on the data that are useful to build upon. 

While there might not be a single algorithm that gives the result wanted, we can research and combine multiple specialized and focused algorithms together to create a single pipeline that solves the higher-level problem. This thesis will focus on such a pipeline and all the supporting algorithms that are involved in this pipeline. How they work individually and how they work together. It is important to understand how each part works to be able to see their limitations and to tweak them to achieve better results.

\subsection{Point Clouds}
This project is interested in collections of 3-dimensional data. The common way of storing 3D information collected from a real environment is with a point cloud data structure. A point cloud is a data structure that contains a collection of multi-dimensional points. This project focuses on 3D point clouds where the points are represented with the X, Y and Z coordinate and, if included, their associated normals. The file format used is the Point Cloud Data format (PCD) \cite{pclabout}. The main advantage for using the PCD format are:
\begin{itemize}
\item The ability to store organized point cloud data sets.
\item Fast loading and saving data to disk.
\item Histogram for feature description support
\end{itemize}
\cite{pclpcd}

\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth,height=6cm,keepaspectratio=true]{img/pointcloudexample}
    \caption{
        Example of a point cloud \cite{pointcloudexample}.
    }
    \label{fig:point cloud example}
\end{figure}

\subsection{Full matching or partial matching}
One way of doing the matching of a scanned environment, which is a small segment of a larger structure, is to match it against a model of the whole complete structure that contains this scanned segment. This approach has the advantage of only needing one large model that can be used to match several environments. This approach also has the advantage that it will require less preparations before it can be used and gives the user more flexibility with what he wants to do a matching on. The large downside is that this approach requires a significantly larger computational power and can introduce considerable more false positives with similar areas of the large model. Because of this problem, full matching will be a difficult approach to implement. The other approach is to only do the matching against a preselected section of the complete model. The partial matching approach is faster and less error prone as there is less data to do the matching against and less false matches as the search has been manually narrowed down. The downside is that you must prepare the selected part you want to match before you can start. Some preparation is not a large problem, it is best to complete a smaller partial matching approach before it should be made complex and difficult for convenience sake.

\subsection{Performance}
Performance is important if this should be able to be used in a real time environment and perhaps be used in an augmented reality device like the Microsoft HoloLens in the future. However, as this is a master project with limited time and resources, researching and implementing an algorithm that shows promise is more important, without too much focus on whether it can run fast enough on a mobile device. But it still important to see if the algorithm can refine and scale down so that is able to be used in real-time. The algorithm must either be fault tolerant enough to be able to handle scans or reconstructions with low enough fidelity or fast enough to be able to handle it with high enough fidelity as scans with high fidelity can be a lot of data to process. 

\subsection{Precision}
The algorithms need to be precise enough to be useful and it is not acceptable to have end results that are only rough estimates. The reason for the need of good precision is that the final application of the 3D matching is that is can be used for inspections of vessels under construction. The algorithm must be able to scale and orient the matched model precisely enough so that it can be useful to compare measurements and details. The algorithm also needs to be able to tackle the matching of objects that have anomalies, occlusions, noise and other artefacts caused by the 3D scanning. The reason for the requirement of robustness is that the scanned data to work with will likely have these issues. It is not common in the literature perform matching with an object scanned from the outside, like a cup or a small object. In our case, the scan and reconstruction of the environment will only have a perspective from the inside of area and with an incomplete understanding of the whole environment.

\subsection{Challenges}
There are a few challenges that governs what algorithms and design choices that can be used for this project. Some can be more relaxed in when not in a production environment and only in the beginning phase. This section will explain some challenges this project has and sets some requirements on the algorithm and design choices done.

\subsubsection{Complexity}
The algorithms handle three-dimensional data are usually complex and computationally heavy as there is a lot of data and in three dimensions. Since these algorithms can be very complex an abstract, it is important to get a deep understanding of how they work to be able to see their limitation and usefulness. Since these algorithms also can be hard to understand completely, it can be difficult to see if they are able to achieve what you want them to do before you have a working implementation to test with. The 3D matching pipeline also need to be able to do several things like identify and recognize the environment characteristics, transform, scale and rotate the virtual model to match the environment. 

\subsubsection{Data quality and characteristics}
The collected point clouds of the environment should be of high enough quality so that the algorithms are able to perform accurate matching. To handle 3D scanned data might have some challenges like occlusion, errors and environment with too few distinct details to separate it from others. Depending on how the environment is structured, you can end up with large occlusions that might make it difficult to do a correct matching. This can be solved by having multiple points where the user scans the environment, however this will drastically increase the data size that the algorithm needs to handle, it also makes the set-up procedure more complicated. To create a pipeline that is quite robust to these challenges will increase the usability and make the matching less complicated to setup and execute.

\subsubsection{Data size}
Capturing high resolution scans is not necessarily a good thing. The density of the point cloud created from a laser scan can be too large. 3D point cloud scans from lasers contain an incredible density that requires a lot of processing to handle. If they are too large one might need to downscale it, but by doing so you also lose details that may be essential to get an accurate match. It will be important to scale down large point cloud with minimal information loss to keep the data usable and smaller for real-time usage where execution time is important.

\subsubsection{Precision}
The 3D matching pipeline needs to be able to perform accurate enough matching and alignment that this can be used in a context where precision is important. To allow flexibility when performing the matching and keep a good precision will in some instances require a separate step that refines the estimated alignments given by the 3D matching pipeline.

\subsubsection{Time}
To implement and test different algorithms and approaches for performing the 3D matching takes a considerable amount of time and this project only has a limited time to be used. Because of this limitation, only a handful of selected approaches and algorithms can be investigated and implemented. Some steps that might only small improvements or is not necessary when assessing the capabilities will not take a priority and only the most essential steps will be implemented and tested.

\end{document}