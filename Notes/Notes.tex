% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{geometry}
\geometry{a4paper}
\usepackage{graphicx}
\usepackage{paralist}
\usepackage{courier}
\usepackage{hyperref}
\usepackage{color}
\usepackage{titling}
\usepackage{array}
\usepackage{listings}
\usepackage{amsmath}

\usepackage[style=numeric-comp]{biblatex}
\bibliography{notes}

\lstnewenvironment{code}[1][]%
  {\noindent\minipage{\linewidth}\medskip 
   \lstset{basicstyle=\ttfamily\footnotesize,frame=single,#1}}
  {\endminipage}

\hypersetup{pdfborder={0 0 0},colorlinks=false,linkcolor=blue,urlcolor=blue,citecolor=blue}


\usepackage{sectsty}
\allsectionsfont{\rmfamily\mdseries\upshape}

\setlength{\droptitle}{-10em}
\title{Notes on implementation \\ \large 3D Matching algorithms}
\author{Matias Snellingen (\emph{matiassn}) \\
\url{matiassn@ifi.uio.no}}

%code quote
%\begin{lstlisting}[basicstyle=\scriptsize]
%\end{lstlisting}
\lstset{language=C++}

\begin{document}

\begin{titlepage}
\maketitle
\vfill
\centering
\includegraphics[width=10cm]{uio.png}
\vfill
\vfill
\end{titlepage}
\tableofcontents

\newpage

\setlength{\parindent}{4em}
\setlength{\parskip}{1em}
\renewcommand{\baselinestretch}{1.5}

\subsection{Converting mesh to point cloud}
Since the algorithm I am going to focus on takes randoms points from the model and measures the distance it makes sens to convert the polygon mesh model to a point cloud for easier compatibility. I first accomplished this by using pcl interfaces that can fetch the cloud version of an mesh. However, this cloud did not have a good distribution of points, this caused the cloud to have very few points across surfaces with little detail. The second attempt was to use a conversion tool named CloudCompare (http://cloudcompare.org) instead to create the point cloud of the model and then use it with PCL. CloudCompare conversion gave much better results and adjustability for the conversion process and result.

\section{Shape distribution}

\subsection{Shape distribution and histograms of geometric statistics}
It is mentioned in the shapedist article that there are other methods that tries to compare discrete histograms of geometric statistics, and some are very similar to the shape distribution method. However these methods only work for manifold meshes and they are sensitive to cracks in the model and small perturbations to the vertices, they are also not invariant under the changes to mesh tessellation. (Check if only the last one or this is for all of them)  Quote from article:

''Other methods have compared discrete histograms of geometric statistics. For example, Thacker et al [1, 4, 8, 9, 25, 26, 47, 54], Huet et al. [33], and Ikeuchi et al. [34] have all represented shapes in 2D images by histograms of angles and distances between pairs of 2D line segments. For 3D shapes, Ankerst et al. [3] has used shape histograms decomposing shells and sectors around a model’s centroid. Besl [13] has considered histograms of the crease anglefor all edges in a 3D triangularmesh. Besl’s method is the most similar to our approach. However, it works only for manifold meshes, it is sensitive to cracks in the models and small perturbations to the vertices, and it is not invariant under changes to mesh tessellation. Moreover, the histogram of crease angles does not always match our intuitive notion of rigid shape. For example, adding any extra arm to a human body results in the same change to a crease angle histogram, no matter whether the new arm extends from the body’s shoulder or the top of its head.''

\subsection{Selection a shape function}
The core of the shape distribution algorithm is shape function and these can have various effect on how effective and robust the algorithm is. The article shapedist mentions that they have tries several different functions, these are:
\begin{itemize}
\item F1: Measures the angle between three random points on the surface of the 3D model.
\item F2: Measures the distance between a fixed point and one random point on the surface.
\item F3: Measures the distance between two random points on the surface.
\item F4: Measures the square root of the area of the triangles between three random points on the surface.
\item F5: Measures the cube root of the volume tetrahedron between four random points on the surface.
\end{itemize}

The articles mentioned that the F3 function that measures the distance between two random points on the surface performs well and have the advantage of simplicity. This is the function that should be implemented first and then we can try others as wee need to do a comparison of the performance when the algorithm is complete.

\subsection{Constructing shape distributions}
We will employ a stochastic method by evaluating $N$ samples from the shape distribution and construct a histogram by counting how many samples fall unto each of $B$ fixed sized bins. From the histogram, we reconstruct a piecewise linear function with $V (\leq B)$ equally spaced vertices, which forms our representation for the shape distribution. We compute the shape distribution once for each model and store it as a sequence of V integers (almost word for word from article).

\subsection{Getting a random sample for the shape function}
We need a good way to get a uniform random sample of points in the cloud we have. For this I found that pcl have a class named pcl::RandomSample that gives me a sample of uniform probability. Based off Algorithm A from the paper "Faster Methods for Random Sampling" by Jeffrey Scott Vitter. The algorithm runs in $O(N)$ and results in sorted indices http://www.ittc.ku.edu/~jsv/Papers/Vit84.sampling.pdf

\subsection{Find the right amount of samples and bins.}
First of all, samples is the amount of points we run the F3 function on, bins is for creating the histogram where we count the samples. When we have this we can reconstruct a piecewise linear function with $V (\leq B)$ equally spaced vertices, which forms our representation for the shape distribution. I start with 1024 samples and 64 bins. 

\subsection{Transfer method to bins}
I frist tried to do the binning method inside the plotting script for gnuplot, but this was not very flexible so I rewrote it in c++ and got a lot better performance. The binning transfer method I do is first to figure out the width of the bins, based on the number of bins $n$ and the $min$ $max$ values of the data range. This is done with this function:
\begin{code}
std::vector<int> bin(std::vector<float>& data, int bins)
{
  float min = *std::min_element(data.begin(), data.end());
  float max = *std::max_element(data.begin(), data.end());
  float width = (max - min) / bins;
  std::vector<int> histogram = std::vector<int>(bins);
  for (const auto& x : data)
  {
    int index = (int)((bins -1) / (max - min) * (x - max) + (bins -1)));
    histogram[index] += 1;
  }
  return histogram;
}
\end{code}

\subsection{Normalization of shapefunction for comparison}
The shape function F2 is insensitive to translation and rotation, it needs to be normalized for scaling so that you can do an comparison. Check 4.3 Comparing Shape Distributions from shapedist. The normalization technique I used was to divide the entries in the histogram by the sum of the histogram. (Area?). This way I should not have problems with scaling messing up the distance between two histograms of different scale.

\subsection{Fitting a piecewise linear function to the data (histogram)}
I will be using residual sum of squares to determine the optimal separation points for the data and then do a linear regression indenpendently on these partitions. This should give us a nice piecewise linear function from the data.



\section{Problems with sampling the model for use with registration and matching}
Virtual point cloud has depth from the walls that does not exist in the raw model because of culling. This might cause problems with the matching as this are features that does not exist in the raw data. Consider culling the model before sampling and/or create a new way to sample them from the inside to get the same culling effect. Also try to sample both the semi raw and virtual the same way.

\section{Correspondence grouping (PCL)}

\subsection{Shape descriptior SHOT}
see: http://robotica.unileon.es/index.php/PCL/OpenNI\_tutorial\_4:\_3D\_object\_recognition\_(descriptors)\#SHOT
SHOT stands for Signature of Histograms of Orientations. It encodes information about the topology (surface) withing a spherical support structure. This sphere is divided in 32 bins or volumes, with 8 divisions along the azimuth, 2 along the elevation, and 2 along the radius. For every volume, a one-dimensional local histogram is computed. The variable chosen is the angle between the normal of the keypoint and the current point within that volume (to be precise, the cosine, which was found to be better suitable).

\subsection{k-d tree}
see: http://robotica.unileon.es/index.php/PCL/OpenNI\_tutorial\_2:\_Cloud\_processing\_(basic)\#k-d\_tree
A k-d tree (k-dimensional tree) is a data structure that organizes a set of points in a k-dimensional space, in a way that makes range search operations very efficient (for example, finding the nearest neighbor of a point, which is the point that is closest to it in space; or finding all neighbors within a radius).

It is a binary tree, that is, every non-leaf node in it has two children nodes, the "left" and "right" ones. Each level splits space on a specific dimension. For example, in 3-dimensional space, at the root node (first level) all children would be split based on the first dimension, X (points having coordinates with greater X values would go to the right subtree, points with lesser values to the left one). At the second level (the nodes we just created), the split would be done on the Y axis, following the same criteria. At the third level (the grandchildren), we would use the Z axis. At the fourth level, we would get back to the X axis, and so on. Usually, the median point is chosen to be root at every level.


\section{Registration}
See: http://robotica.unileon.es/index.php/PCL/OpenNI\_tutorial\_3:\_Cloud\_processing\_(advanced)\#Registration
using registration, the same used to stich together different point clouds to get the transformation matrix to align the model with the virtual cloud model. There are two main methods that are worth a look.

\subsection{ICP registration}
This is the brutforce way to do it, and it might not be suitable for us as: "ICP will probably fail if the difference between the clouds is too big. Usually, you will use features first to perform an initial rough alignment of the clouds, and then use ICP to refine it with precision. " 

\subsection{Feature-based registration}
This one is similar to correspondence grouping as it does much of the same steps. 

\subsection{Problems with depth or volume to the virtual model.}
Having volulme in the walls of the virtual model causes problems when we try to match the reconstructed model with it, since it does not have this feature. 


\newpage
\nocite{*}

\printbibliography

\end{document}
